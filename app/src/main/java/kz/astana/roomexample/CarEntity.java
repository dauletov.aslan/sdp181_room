package kz.astana.roomexample;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "cars")
public class CarEntity {
    @PrimaryKey(autoGenerate = true)
    public long id;
    @ColumnInfo(name = "brand_name")
    public String brand;
    @ColumnInfo(name = "model_name")
    public String model;
    public String year;
}
