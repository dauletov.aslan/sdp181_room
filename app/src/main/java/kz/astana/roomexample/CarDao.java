package kz.astana.roomexample;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface CarDao {

    @Query("SELECT * FROM cars")
    List<CarEntity> getAll();

    @Query("SELECT * FROM cars WHERE id = :id")
    CarEntity getById(long id);

    @Insert
    void insert(CarEntity... car);

    @Insert
    void insert(List<CarEntity> cars);

    @Update
    void update(CarEntity car);

    @Delete
    void delete(CarEntity car);
}