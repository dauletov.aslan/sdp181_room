package kz.astana.roomexample;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {CarEntity.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CarDao carDao();
}
