package kz.astana.roomexample;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private CarDao carDao;
    private ArrayAdapter<String> adapter;
    private EditText idEditText, brandEditText, modelEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        carDao = App.instance.getDatabase().carDao();

        ListView listView = findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, new ArrayList<>());
        listView.setAdapter(adapter);

        idEditText = findViewById(R.id.idEditText);
        brandEditText = findViewById(R.id.brandEditText);
        modelEditText = findViewById(R.id.modelEditText);

        Button load = findViewById(R.id.loadButton);
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatabaseTask().execute(Command.LOAD);
            }
        });
        Button byId = findViewById(R.id.byIdButton);
        byId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatabaseTask().execute(Command.BY_ID);
            }
        });
        Button insert = findViewById(R.id.insertButton);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatabaseTask().execute(Command.INSERT);
            }
        });
        Button update = findViewById(R.id.updateButton);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatabaseTask().execute(Command.UPDATE);
            }
        });
        Button delete = findViewById(R.id.deleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatabaseTask().execute(Command.DELETE);
            }
        });
    }

    private class DatabaseTask extends AsyncTask<Command, Void, List<CarEntity>> {

        private String id, brand, model;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            id = idEditText.getText().toString();
            brand = brandEditText.getText().toString();
            model = modelEditText.getText().toString();
        }

        @Override
        protected List<CarEntity> doInBackground(Command... commands) {
            switch (commands[0]) {
                case LOAD:
                    return carDao.getAll();
                case BY_ID:
                    CarEntity car = carDao.getById(Long.parseLong(id));
                    ArrayList<CarEntity> cars = new ArrayList<>();
                    cars.add(car);

                    return cars;
                case INSERT:
                    CarEntity carEntity = new CarEntity();
                    carEntity.brand = brand;
                    carEntity.model = model;

                    carDao.insert(carEntity);
                    break;
                case UPDATE:
                    CarEntity updateEntity = new CarEntity();
                    updateEntity.id = Long.parseLong(id);
                    updateEntity.brand = brand;
                    updateEntity.model = model;

                    carDao.update(updateEntity);
                    break;
                case DELETE:
                    CarEntity deleteEntity = new CarEntity();
                    deleteEntity.id = Long.parseLong(id);

                    carDao.delete(deleteEntity);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<CarEntity> cars) {
            super.onPostExecute(cars);

            if (cars != null) {
                adapter.clear();
                ArrayList<String> carList = new ArrayList<>();
                for (CarEntity car : cars) {
                    carList.add("ID: " + car.id + ", " + car.brand + " " + car.model);
                }
                adapter.addAll(carList);
            }
        }
    }

    private enum Command {
        LOAD, BY_ID, INSERT, UPDATE, DELETE
    }
}